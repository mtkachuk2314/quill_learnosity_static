import React, { useState } from "react";
import MultipleChoice from "./components/MultipleChoice/MultipleChoice";
import Text from "./components/Text/Text";

function App() {
  const [components, setComponents] = useState([]);

  const addComponent = (component) => {
    setComponents((prev) => [...prev, component]);
  };

  return (
    <div className="app">
      <div className="side-nav">
        <button onClick={() => addComponent("MultipleChoice")}>
          Multiple Choice
        </button>
        <button onClick={() => addComponent("Text")}>Text</button>
      </div>
      <div className="main-section">
        {components.map((component, index) => {
          if (component === "MultipleChoice") {
            return <MultipleChoice key={index} />;
          } else if (component === "Text") {
            return <Text key={index} />;
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
}

export default App;
